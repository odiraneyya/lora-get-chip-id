uint64_t chipid;  

void setup() {
	Serial.begin(115200);
}

void loop() {
	chipid=ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).

  int j = 0;
  char deveui_hex_bytes[][3] = {"", "", "", "fe", "ff", "", "", ""};
  while (chipid != 0) {
    while ('\0' != *deveui_hex_bytes[j]) j++;
    sprintf(deveui_hex_bytes[j], "%02x", (uint8_t) chipid);
    chipid >>= 8;
  }

  Serial.print("DEVEUI<HEX ARRAY> = { ");
  for (int i = 7; i >= 0; --i) {
    Serial.print("0x");
    Serial.print(deveui_hex_bytes[i]);
    if (0 != i)
      Serial.print(", ");
  }
  Serial.print(" }\nDEVEUI<HEX STRING> = ");
  for (int i = 7; i >= 0; --i)
    Serial.print(deveui_hex_bytes[i]);

  Serial.print("\n");
  
	delay(10000);

}
